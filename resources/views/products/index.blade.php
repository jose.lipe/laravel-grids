@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Listagem de Produtos</h3>
        @include('table.table')
    </div>
@endsection