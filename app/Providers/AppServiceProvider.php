<?php

namespace App\Providers;

use Faker\Generator;
use Faker\Factory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->singleton(Generator::class, function () {
                return Factory::create(env('FAKER_LANGUAGE'));
            });
        }
    }
}
